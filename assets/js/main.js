// Namespace wrapped to prevent public exposure
var fddm = function() {
    var bars;
    var round = [];
    function init() {
        $.get(config.serviceURL + "getBars", function(data){
            bars = data;
            var location = new google.maps.LatLng(bars[0].lat,bars[0].long);

            var mapoptions = {
                center: location,
                zoom: 14
            };

            var var_map = new google.maps.Map(document.getElementById("map-container"),mapoptions);
            $.each(bars, function(index,bar) {
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(bar.lat,bar.long),
                    map: var_map,
                    title: bar.name,
                    icon: "http://maps.google.com/mapfiles/marker" + bar.name.charAt(0) + ".png",
                    animation: google.maps.Animation.DROP,
                    bar: bar});
                marker.setMap(var_map);	
                google.maps.event.addDomListener(marker, 'click', function() {
                    var bar_id = marker.bar.id;
                    $('#map-panel').hide();
                    $('#bar-panel').show();
                    $('#back-link').show();
                    $('#drinks-container').empty();
                    var stateObj = {}
                    // Update state to allow back button
                    history.pushState(stateObj, "page2", "");
                    $('#page-title').html(marker.bar.name + ": Choose drinks and add them to the order");
                    $.get(config.serviceURL + "getProducts", function(data) {
                        drinks = data;
                        $.get(config.serviceURL + "getPrices", {"bar_id":bar_id}, function(price_data) {
                            $.each(drinks, function(index,drink) {
                                var price = $.grep(price_data, function(e) {
                                    return (e.product_id == drink.id);
                                })[0];
                                if (price) {
                                    drink.price = price.current_price;
                                    $('#drinks-container').append("<div class='drink-item'><div class='drink-title text-center'>" + drink.name + "</div><div class='text-center drink-img'><img data-name='" + drink.name + "' data-id='" + drink.id + "' data-price='" + drink.price + "' src='" + drink.image_url + "' title=' + " + drink.name + "' /></div><div class='drink-price text-center'>" + drink.price.toFixed(2) + "</div></div>");
                                }
                            });
                            $('.drink-img img').click(function(e) {
                                var drink_id = $(this).data('id');
                                var curr_drink = $.grep(drinks, function(e) {
                                    return (e.id.toString() == drink_id);
                                })[0];
                                if (typeof curr_drink.quantity === "undefined")
                                    curr_drink.quantity = 1;
                                var contains = $.grep(round, function(e) {
                                    return (e.id.toString() == drink_id);
                                });
                                if (contains.length > 0)
                                    contains[0].quantity++;
                                else
                                    round.unshift(curr_drink);
                                redrawRound();
                            });
                        });
                    });
                })
            });
        });
        $('#back-link').click(goBack);
    }
    function goBack() {
            clearRound();
            $('#page-title').html("Select a bar from the map");
            $('#bar-panel').hide();
            $('#back-link').hide();
            $('#map-panel').show();
    }

    // Clear the round data and UI
    function clearRound() {
        round = [];
        $('#order-container').empty();
    }

    // Redraw the round UI
    function redrawRound() {
        $('#order-container').empty();
        var total = 0;
        $.each(round, function(index,item) {
            var drink_cont = "<div class='row'><div class='col-xs-4 drink-quant'>" + item.quantity + "</div><div class='col-xs-4 drink-name'>" + item.name + "</div><div class='col-xs-2 drink-price'>" + (item.price*item.quantity).toFixed(2) + "</div><div class='reduce col-xs-2'><a href='javascript:fddm.reduceRound(" + item.id + ");'>-</div></div>";
            $('#order-container').append(drink_cont);
            total += (item.price*item.quantity);
        });
        $('#order-container').append("<div class='row'><div class='text-right col-xs-8'><strong>Total:</strong></div><strong><div class='col-xs-2 drink-price'>" + total.toFixed(2) + "</div></strong><div class='col-xs-2'><a href='javascript:fddm.clearRound();'>Clear</a></div></div>");
    }

    // Reduce a round item by one and remove if now == 0
    function reduceRound(id) {
        var contains = $.grep(round, function(e) {
            return (e.id.toString() == id.toString());
        })[0];
        if (contains.quantity>1)
            contains.quantity--;
        else {
            round = round.filter(function(el) {
                return el.id !== id;
            })
        }
        if (round.length > 0)
            redrawRound();
        else
            clearRound();
    }
    // Public function exposure
    return {
        reduceRound:reduceRound,
        clearRound:clearRound,
        goBack:goBack,
        init:init
    }
}();
$(function() {
    google.maps.event.addDomListener(window, 'load', fddm.init);
    // Trap back button usage.
    window.onpopstate = function(event) {
        fddm.goBack();
    };
});