# README #

FanDuel Roundminder is a mobile web application for remembering drink orders in a pub or bar.

## Setup ##

Inside the service folder is a node application which serves a simple API which currently deals with passing bar, product and pricing information for the dummy data. It runs over port 3000 but it would be advisable to route that port via nginx/apache to operate over port 80 for production use.

```
node app.js
```

starts the node server and sets it listening on port 3000.

## Config ##

The file assets/js/config.js contains an object where the node server/port can be setup and should be configured before testing.

```javascript
config = {
    serviceURL: "http://192.168.1.120:3000/",
    version: "0.1"
}
```

## Dependencies ##

Server side:

* nodeJS (coded with 6.9.5, should work with any version)
* express (included) - for simple REST handling
* lodash (included) - for data searching an manipulation

Client libraries:

* jQuery (included) - better DOM manipulation
* Bootstrap (included) - for speed of UI prototyping
* Google Maps API (linked) - for mapping interface

## Rest API ##

Calls that are currently included in the sample API are as follows

```
getBars - returns all the bars in the data sample.
getProducts - returns all products in the data sample.
getPrices - returns prices filtered by bar_id and/or product_id
  optional params: {"bar_id":int, "product_id":int}
```

Future developments of the API should include the ability to store round details and being able to recall previously saved rounds.