var express = require('express');
var fs = require('fs');
var app = express();
var _ = require('lodash');

process.on('uncaughtException', function (err) {
  console.log('Caught exception: ' + err.stack);
});
app.use(function (req,res,next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  next();
});

app.get('/test', function(req, res) {
    res.send("<html><head></head><body>Node server running</body></html>");
});
app.get('/getBars', function(req, res) {
    res.sendFile(__dirname +"/data/bars.json");
});
app.get('/getPrices', function(req, res) {
    var bar_id = parseInt(req.query.bar_id);
    var product_id = parseInt(req.query.product_id);
    var results = [];
    if (bar_id && product_id) {
        readJSONFile("data/prices.json", function(err, json) {
        results = _.filter(json, {'bar_id':bar_id});
            results = _.filter(results, {'product_id':product_id});
            res.send(results);
        });
    }
    else if (bar_id) {
        readJSONFile("data/prices.json", function(err, json) {
            results = _.filter(json, {'bar_id':bar_id});
            res.send(results);
        });
    }
    else {
        res.sendFile(__dirname + "/data/prices.json");
    }
});
app.get('/getProducts', function(req,res) {
    res.sendFile(__dirname + "/data/products.json");
})

app.get('/', function(req, res) {
	res.send('');
});
app.get('/')
app.listen(3000, function() {
	console.log("Listening on 3000");
});

function readJSONFile(filename, callback) {
  fs.readFile(filename, function (err, data) {
    if(err) {
      callback(err);
      return;
    }
    try {
      callback(null, JSON.parse(data));
    } catch(exception) {
      callback(exception);
    }
  });
}